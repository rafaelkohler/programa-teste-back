package br.com.programateste.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.programateste.entity.Veiculo;
import br.com.programateste.services.VeiculoService;

@RestController
@RequestMapping(value = "/veiculos")
public class VeiculoResource {

	@Autowired
	private VeiculoService veiculoService;

	@RequestMapping(value = "/findbymodelo/{modelo}", method = RequestMethod.GET)
	public ResponseEntity<Veiculo> findByModelo(@PathVariable String modelo) {
		Veiculo obj = veiculoService.findByModelo(modelo);
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(value = "/findbyplaca/{placa}", method = RequestMethod.GET)
	public ResponseEntity<Veiculo> findByPlaca(@PathVariable String placa) {
		Veiculo obj = veiculoService.findByPlaca(placa);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Veiculo>> findAll() {
		List<Veiculo> listVeiculos = veiculoService.findAll();
		return ResponseEntity.ok().body(listVeiculos);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Veiculo> find(@PathVariable Integer id) {
		Veiculo obj = veiculoService.find(id);
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody Veiculo veiculo) {
		veiculo = veiculoService.insert(veiculo);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(veiculo.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody Veiculo veiculo, @PathVariable Integer id) {
		Veiculo obj = veiculo;
		obj.setId(id);
		obj = veiculoService.update(obj);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Veiculo> delete(@PathVariable Integer id) {
		veiculoService.delete(id);
		return ResponseEntity.noContent().build();
	}

}