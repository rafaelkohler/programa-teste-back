package br.com.programateste.resources;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.programateste.entity.Funcionario;
import br.com.programateste.services.FuncionarioService;

@RestController
@RequestMapping(value = "/funcionarios")
public class FuncionarioResource {

	@Autowired
	private FuncionarioService funcionarioService;

	@RequestMapping(value = "/findbynome/{nome}", method = RequestMethod.GET)
	public ResponseEntity<Funcionario> findByNome(@PathVariable String nome) {
		Funcionario obj = funcionarioService.findByNome(nome);
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(value = "/findbycpf/{cpf}", method = RequestMethod.GET)
	public ResponseEntity<Funcionario> findByCpf(@PathVariable String cpf) {
		Funcionario obj = funcionarioService.findByCpf(cpf);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value = "/findbetweenanivesario/{startDate}/{endDate}", method = RequestMethod.GET)
	public ResponseEntity<List<Funcionario>> findByAniversarioBetween(@PathVariable String startDate, @PathVariable String endDate) {
		Date inicio = null;
		Date fim = null;
		try {
			inicio = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(startDate);
			fim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<Funcionario> obj = funcionarioService.findByBetween(inicio, fim);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Funcionario>> findAll() {
		List<Funcionario> listFuncionario = funcionarioService.findAll();
		return ResponseEntity.ok().body(listFuncionario);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Funcionario> find(@PathVariable Integer id) {
		Funcionario obj = funcionarioService.find(id);
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody Funcionario funcionario) {
		funcionario = funcionarioService.insert(funcionario);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(funcionario.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody Funcionario funcionario, @PathVariable Integer id) {
		Funcionario obj = funcionario;
		obj.setId(id);
		obj = funcionarioService.update(obj);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Funcionario> delete(@PathVariable Integer id) {
		funcionarioService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
