package br.com.programateste.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.programateste.entity.Veiculo;

@Repository
public interface VeiculoRepository extends JpaRepository<Veiculo, Integer> {
	
	@Transactional(readOnly=true)
	Veiculo findByPlaca(String placa);
	
	@Transactional(readOnly=true)
	Veiculo findByModelo(String modelo);

}
