package br.com.programateste.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.programateste.entity.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer> {
	
	@Transactional(readOnly=true)
	Funcionario findByCpf(String cpf);
	
	@Transactional(readOnly=true)
	Funcionario findByNome(String nome);
	
	@Query(value = "select f from Funcionario f where f.dataNascimento BETWEEN :startDate AND :endDate")
	public List<Funcionario> getAllBetweenDates(@Param("startDate")Date startDate,@Param("endDate")Date endDate);

}
