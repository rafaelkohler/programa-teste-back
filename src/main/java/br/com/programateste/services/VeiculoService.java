package br.com.programateste.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.programateste.entity.Veiculo;
import br.com.programateste.repositories.VeiculoRepository;
import br.com.programateste.services.exceptions.ObjectNotFoundException;

@Service
public class VeiculoService {

	@Autowired
	private VeiculoRepository veiculoRepository;

	public Veiculo find(Integer id) throws ObjectNotFoundException {
		Optional<Veiculo> obj = veiculoRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Veiculo.class.getName()));
	}

	@Transactional
	public Veiculo insert(Veiculo veiculo) {
		veiculo.setId(null);
		return veiculoRepository.save(veiculo);
	}

	public Veiculo update(Veiculo veiculo) {
		return veiculoRepository.save(veiculo);
	}

	public void delete(Integer id) {
		find(id);
		veiculoRepository.deleteById(id);
	}

	public List<Veiculo> findAll() {
		return veiculoRepository.findAll();
	}
	
	public Veiculo findByModelo(String modelo) throws ObjectNotFoundException {
		Veiculo veiculo = veiculoRepository.findByModelo(modelo);
		if (veiculo == null) {
			throw new ObjectNotFoundException("Veículo no modelo: " + modelo + " não foi encontrado.");
		}
		return veiculo;
	}
	
	public Veiculo findByPlaca(String placa) throws ObjectNotFoundException {
		Veiculo veiculo = veiculoRepository.findByPlaca(placa);
		if (veiculo == null) {
			throw new ObjectNotFoundException("Veículo de placa: " + placa + " não foi encontrado.");
		}
		return veiculo;
	}

}
