package br.com.programateste.services;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.programateste.entity.Funcionario;
import br.com.programateste.entity.Veiculo;
import br.com.programateste.repositories.FuncionarioRepository;
import br.com.programateste.repositories.VeiculoRepository;

@Service
public class DBService {
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;
	@Autowired
	private VeiculoRepository veiculoRepository;
	
	public void instantiateDataBase() throws ParseException {
		Funcionario f1 = new Funcionario(null, "48582168020", "Joaquim", new Date(), "joaquim123", "123");
		Funcionario f2 = new Funcionario(null, "06132847049", "Chin Sakchai", new Date(), "chin123", "123");
		Funcionario f3 = new Funcionario(null, "35251023057", "Joana Ramlah", new Date(), "joana123", "123");
		Funcionario f4 = new Funcionario(null, "59930726012", "Marcela Caratacus", new Date(), "marcela123", "123");
		
		funcionarioRepository.saveAll(Arrays.asList(f1, f2, f3, f4));
		
		Veiculo v1 = new Veiculo(null, "SDE-3425", "sim", 2016, 2016, "chassiteste9458", new Date(), new Date(), "Carro", "azul", 10d, 5);
		Veiculo v2 = new Veiculo(null, "JGL-5487", "não", 2000, 2001, "chassiteste654154", new Date(), new Date(), "Caminão", "Vermelhor", 3.5d, 5);
		veiculoRepository.saveAll(Arrays.asList(v1, v2));
	}

}
