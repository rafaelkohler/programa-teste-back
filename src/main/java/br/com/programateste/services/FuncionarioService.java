package br.com.programateste.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.programateste.entity.Funcionario;
import br.com.programateste.repositories.FuncionarioRepository;
import br.com.programateste.services.exceptions.ObjectNotFoundException;

@Service
public class FuncionarioService {

	@Autowired
	private FuncionarioRepository funcionarioRepository;

	public Funcionario find(Integer id) throws ObjectNotFoundException {
		Optional<Funcionario> obj = funcionarioRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Funcionario.class.getName()));
	}

	@Transactional
	public Funcionario insert(Funcionario funcionario) {
		funcionario.setId(null);
		return funcionarioRepository.save(funcionario);
	}

	public Funcionario update(Funcionario funcionario) {
		return funcionarioRepository.save(funcionario);
	}

	public void delete(Integer id) {
		find(id);
		funcionarioRepository.deleteById(id);
	}

	public List<Funcionario> findAll() {
		return funcionarioRepository.findAll();
	}
	
	public Funcionario findByCpf(String cpf) throws ObjectNotFoundException {
		Funcionario funcionario = funcionarioRepository.findByCpf(cpf);
		if (funcionario == null) {
			throw new ObjectNotFoundException("Funcionário não encontrado, verifique se o CPF está correto: " + cpf);
		}
		return funcionario;
	}
	
	public List<Funcionario> findByBetween(Date startDate, Date endDate) throws ObjectNotFoundException {
		List<Funcionario> funcionarios = funcionarioRepository.getAllBetweenDates(startDate, endDate);
		if (funcionarios == null) {
			throw new ObjectNotFoundException("Funcionário não encontrado, verifique se o CPF está correto: " + startDate);
		}
		return funcionarios;
	}
	
	public Funcionario findByNome(String nome) throws ObjectNotFoundException {
		Funcionario funcionario = funcionarioRepository.findByNome(nome);
		if (funcionario == null) {
			throw new ObjectNotFoundException("Funcionário não encontrado, verifique se o nome está correto: " + nome);
		}
		return funcionario;
	}

}
