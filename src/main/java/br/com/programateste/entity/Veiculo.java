package br.com.programateste.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Veiculo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Size(max=10)
	private String placa;
	
	@NotNull
	private String ativo;
	
	@NotNull
	private Integer anoFabricacao;
	
	@NotNull
	private Integer anoModelo;
	
	@NotNull
	private String chassi;
	
	@Temporal(TemporalType.TIMESTAMP)
//	@JsonFormat(pattern="dd/MM/yyyy")
	private Date dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
//	@JsonFormat(pattern="dd/MM/yyyy")
	private Date dataDesativacao;
	
	@NotNull
	@Size(max=30)
	private String modelo;
	
	@Size(max=20)
	private String cor;
	
	@NotNull
	private Double consumoMedioPorKm;
	
	@NotNull
	private Integer quantidadePassageiro;
	
	public Veiculo() {
		
	}

	public Veiculo(Integer id, String placa, String ativo, Integer anoFabricacao, Integer anoModelo, String chassi,
			Date dataCadastro, Date dataDesativacao, String modelo, String cor, Double consumoMedioPorKm,
			Integer quantidadePassageiro) {
		super();
		this.id = id;
		this.placa = placa;
		this.ativo = ativo;
		this.anoFabricacao = anoFabricacao;
		this.anoModelo = anoModelo;
		this.chassi = chassi;
		this.dataCadastro = dataCadastro;
		this.dataDesativacao = dataDesativacao;
		this.modelo = modelo;
		this.cor = cor;
		this.consumoMedioPorKm = consumoMedioPorKm;
		this.quantidadePassageiro = quantidadePassageiro;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Integer getAnoModelo() {
		return anoModelo;
	}

	public void setAnoModelo(Integer anoModelo) {
		this.anoModelo = anoModelo;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataDesativacao() {
		return dataDesativacao;
	}

	public void setDataDesativacao(Date dataDesativacao) {
		this.dataDesativacao = dataDesativacao;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Double getConsumoMedioPorKm() {
		return consumoMedioPorKm;
	}

	public void setConsumoMedioPorKm(Double consumoMedioPorKm) {
		this.consumoMedioPorKm = consumoMedioPorKm;
	}

	public Integer getQuantidadePassageiro() {
		return quantidadePassageiro;
	}

	public void setQuantidadePassageiro(Integer quantidadePassageiro) {
		this.quantidadePassageiro = quantidadePassageiro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veiculo other = (Veiculo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Veiculo [id=" + id + ", placa=" + placa + ", modelo=" + modelo + "]";
	}

}
